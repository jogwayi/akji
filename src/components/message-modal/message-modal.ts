import { Component } from '@angular/core';
import { ViewController ,NavParams} from 'ionic-angular';
/*
  Generated class for the MessageModal component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'message-modal',
  templateUrl: 'message-modal.html'
})
export class MessageModalComponent {

  buttonText:string='Ok';
  title:string='Ok';
  message:string='';

  constructor(private params:NavParams,private viewCtrl: ViewController) {
    this.title = this.params.data['title'];
    this.message = this.params.data['message'];
  }
  dismiss(){
    this.viewCtrl.dismiss({done:true});
  }

}
