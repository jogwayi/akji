import { Component } from '@angular/core';

/*
  Generated class for the EmailValidator component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'email-validator',
  templateUrl: 'email-validator.html'
})
export class EmailValidatorComponent {

  text: string;

  constructor() {}

  searchPlaces(lat,lng) {
      //use the pure js method to get places
      var pyrmont = {lat: lat, lng: lng};
      this.placeholderMap = new google.maps.Map(document.getElementById('gmp'), {
          center: pyrmont,
          zoom: 17
        });
        var service = new google.maps.places.PlacesService(this.placeholderMap);
        service.nearbySearch({
          location: pyrmont,
          radius: 500,
          type: ['point_of_interest']
        }, (results, status, pagination) =>{
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
              return;
            } else {
              console.log(results);
              this.addMarkers(results)
            }
          });      
      /*
      should handle cors
      let url = this.placesUrl+'=-33.8670522,151.1957362&radius=500&type=point_of_interest&key='+this.apiKey;
      this.http.get(url)
      .map(res=>res.json() || {})
      .subscribe(res=>{
          alert(res);
      });*/

  }
  
}
