import { Component } from '@angular/core';

/*
  Generated class for the EmailSent component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'email-sent',
  templateUrl: 'email-sent.html'
})
export class EmailSentComponent {

  //variables
  title: string;
  line1: string;
  line2: string;
  line3: string;
  buttonText: string;

  constructor() {
    console.log('Hello EmailSent Component');

    this.title="Almost Finished...";
    this.line1="We need to confirm your email address.";
    this.line1="We've sent an email to ";
    this.line1="Please click the link in that messageto activate your account.";
    this.buttonText="OK";
  }
}
