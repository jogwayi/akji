import { Component } from '@angular/core';
import { NavController,Platform,ViewController,NavParams,ToastController,ModalController,Loading,LoadingController } from 'ionic-angular';
import { HttpRequestService } from '../../providers/http-request';
import { MessageModalComponent,EmailVerificationComponent} from '../../components/components-barrel';
import { SQLite } from "ionic-native";


/*
  Generated class for the ChangeEmail component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'change-email',
  templateUrl: 'change-email.html'
})
export class ChangeEmailComponent {

 title:string='Almost Finished!';
  message:string='';
  email:string;
  loader:Loading;

  registrationStatusId:number;
  //$text-color: #000099
  constructor(private navCtrl: NavController,private toastCtrl: ToastController,private loadingCtrl:LoadingController,private viewCtrl: ViewController,params:NavParams,private modal:ModalController,private httpService:HttpRequestService,private platform:Platform) {
    this.title = params.data['title'];    
    this.title = params.data['message'];    
    this.registrationStatusId=1;
  }
   
  dismiss(){
    this.viewCtrl.dismiss({done:true,title:'Email Verification!',message:'Ensure you click on the email verification in your inbox to activate your email address'});
    this.platform.exitApp();
  }
  saveEmail(){
    this.loader = this.loadingCtrl.create({
      content:"Changing you email address...\n\nPlease wait",
    });
    this.loader.present();
    this.httpService.changeEmailAndSendVerificationMail(
        {NewEmailAddress:this.email}
      ,{}).subscribe(res=>{
          this.loader.dismiss();
          if(res.Data.ActionStatus){
            let db=new SQLite();
            db.openDatabase({
                name: 'akjistore.db',
                location: 'default' // the location field is required
              }).then(() => {
                db.executeSql(
                      'UPDATE tbluser SET email=?',
                      [this.email]                  
                    ).then(() => {
                          this.verify("Email Changed!","An email has been sent to " + this.email + ". Please click the link in that message to activate your account");                          




                      //this.navCtrl.setRoot(LaunchPage,{RegistrationStatusId:this.registrationStatusId,imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
                      //this.viewCtrl.dismiss({done:true,title:'Email Changed!',message:res.Data.ActionMessage});

                      //this.navCtrl.setRoot(HomePage,{});
                  }, (err) => {
                    console.log("Could not save new email");
                  //this.loader.setContent('Unable to execute data: '+ err);
                });
                //this.navCtrl.setRoot(HomePage,{});
                //this.viewCtrl.dismiss({done:true,title:'Email Changed!',message:res.Data.ActionMessage});
                this.verify("Email Changed!","An email has been sent to " + this.email + ". Please click the link in that message to activate your account"); 
            }, (err) => {
              //this.loader.setContent('Unable to open database: '+ err);
            });

              //this.viewCtrl.dismiss({done:true,title:'Email Changed!',message:res.Data.ActionMessage});
              //this.verify("Email Changed!","An email has been sent to " + this.email + ". Make sure to check your junk or spam folder. The sender is verification@akji-kenya.org");                          

          }else{
              let toast = this.toastCtrl.create({
                message: res.Data.ActionMessage,
                position: 'bottom',
                showCloseButton:true
              });
              toast.present();
              toast.onDidDismiss(() => {
                console.log('Message received');
              });
          }
      });
  }
   verify(title:string,message:string,done:boolean=false){
      let alertModal = this.modal.create(MessageModalComponent,{title:title,message:message});
      alertModal.present();
      alertModal.onDidDismiss((data)=>{
        this.platform.exitApp();
      })
  }
}