import { Component } from '@angular/core';
import {Platform,NavController, ViewController,NavParams,ToastController,Loading,LoadingController } from 'ionic-angular';
import { HttpRequestService } from '../../providers/http-request';
@Component({
  selector: 'email-verification',
  templateUrl: 'email-verification.html'
})
export class EmailVerificationComponent {

  text: string;
  buttonText:string='EXIT';
  title:string='Email Verification Required!';
  message:string='Please click the link in the email message to confirm your email address';
  senderEmail:string='verification@akji-kenya.org';
  loader:Loading;
  verificationData:{CountryCode:string,
          MobileNumber:string,
          EmailAddress:string,
          fcmid:string,
          mcc:string,
          DeviceId:string}={CountryCode:'',
          MobileNumber:'',
          EmailAddress:'',
          fcmid:'',
          mcc:'',
          DeviceId:''};
  constructor(private navCtrl: NavController,private platform:Platform,private toastCtrl: ToastController,private loadingCtrl:LoadingController,private httpService:HttpRequestService,private params:NavParams,private viewCtrl: ViewController) {
        this.verificationData = params.data['data'];
    
  }
  resendMessage(){
    this.loader = this.loadingCtrl.create({
      content:"Registering you...\n\nPlease wait",
    });
    this.loader.present();
    this.httpService.resendVerificationMail(
        {}
      ,{}).subscribe(res=>{
          this.loader.dismiss();
          if(res.Data.ActionStatus){
              this.viewCtrl.dismiss({done:true});
          }else{
              let toast = this.toastCtrl.create({
                message: res.Data.ActionMessage,
                position: 'bottom',
                showCloseButton:true
              });
              toast.present();
              toast.onDidDismiss(() => {
                console.log('Message received');
              });
          }
      });
  }
  changeMail(){
    this.viewCtrl.dismiss({done:false,changeMail:true});
  }
  dismiss(){
    /* this.loader = this.loadingCtrl.create({
        content: "Validating your information...",
      });
      this.loader.present();
      this.checkRegistrationStatus();*/
      this.platform.exitApp();
       /* this.httpService.validatePhoneNumber(
          {CountryCode:this.verificationData.CountryCode,
          MobileNumber:this.verificationData.MobileNumber,
          EmailAddress:this.verificationData.EmailAddress,
          fcmid:this.verificationData.fcmid,
          mcc:this.verificationData.mcc,
          DeviceId:this.verificationData.DeviceId}
        ,{}).subscribe(res=>{
          this.loader.dismiss();
            if(res.Data.UserStatus==1 || res.Data.UserStatus==2){
                  this.navCtrl.setRoot(SignUpPage,{imei:this.verificationData.DeviceId,countryCode:this.verificationData.CountryCode,mcc:this.verificationData.mcc,emailAddress: this.verificationData.EmailAddress,mobileNumber :this.verificationData.MobileNumber});
            }else{
                this.platform.exitApp();
            }

        });*/
    //this.viewCtrl.dismiss({done:true});
  }
  
}
