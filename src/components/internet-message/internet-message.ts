import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

/*
  Generated class for the InternetMessage component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'internet-message',
  templateUrl: 'internet-message.html'
})
export class InternetMessageComponent {

  title:string='No Internet Connection!';
  message:string='Your internet connection is off. Please turn it on and try again.';
  //$text-color: #000099
  constructor(private viewCtrl: ViewController) {
    console.log('Hello InternetMessage Component');
    
  }
  turnOnWifi(){
      if(typeof window['cordova'].plugins.settings.openSetting != undefined){
           window['cordova'].plugins.settings.open(()=>{
            console.log("opened settings")
        },
        ()=>{
            console.log("failed to open settings")
        });
      }
  }
  turnOnData(){
      if(typeof window['cordova'].plugins.settings.openSetting != undefined){
           window['cordova'].plugins.settings.open(()=>{
            console.log("opened settings")
        },
        ()=>{
            console.log("failed to open settings")
        });
      }
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }

}
