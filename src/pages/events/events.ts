import { Component } from '@angular/core';
import { NavController,Loading,LoadingController,NavParams } from 'ionic-angular';
import { HttpRequestService } from '../../providers/http-request'; 
import { EventsDetailPage } from '../events-detail/events-detail';

 export class NormalEvents{
      EventID: number;
      EventTitle:string;
      EventDescription:string;
      StartDateTime:string;
      EventDate:string;
      EndDateTime:string;
      DurationInHrs:string;
      BookingExpiryDateTime:string;
      EventType:string;
      City:string;
      Venue:string;
      Committee:string;
      ThematicArea:string;
      BookingType:string;

      constructor(options:{EventID: number,EventTitle:string,
            EventDescription:string, StartDateTime?:string,  
            EndDateTime?:string, EventDate?:string, DurationInHrs?:string,
            BookingExpiryDateTime?:string,EventType?:string,
            City?:string, Venue?:string,Committee?:string, ThematicArea?:string,
            BookingType?:string}){
            this. EventID =options.EventID;
            this.EventTitle=options.EventTitle;
            this.EventDescription=options.EventDescription;
              this.StartDateTime=options.StartDateTime;
              this.EventDate=options.EventDate;
              this.EndDateTime=options.EndDateTime;
              this.DurationInHrs=options.DurationInHrs;
              this.BookingExpiryDateTime=options.BookingExpiryDateTime;
              this.EventType=options.EventType;
              this.City=options.City;
              this.Venue=options.Venue;
              this.Committee=options.Committee;
              this.ThematicArea=options.ThematicArea;
              this.BookingType=options.BookingType;


          }

}

@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage {

  events:Array<NormalEvents>=[];
  title:string;
  loader:Loading;
  constructor(private httpService:HttpRequestService,private loadingCtrl: LoadingController,public navCtrl: NavController,private params:NavParams) {
      this.events= params.data['events'];
      this.title= params.data['title'];
  }
  showDetail(EventID,title){
    this.loader = this.loadingCtrl.create({content:'Loading event...'});
     this.loader.present();
      this.httpService.getNormalEventDetail({
          EventID:EventID
        },{}).subscribe(res=>{   
            this.loader.dismiss();
            if(res.Data){
              this.navCtrl.push(EventsDetailPage,{title:title,event:res.Data});
            }
      })
     
  }
  ionViewDidLoad() {
     
  }

}

