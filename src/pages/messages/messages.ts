import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {SQLite,LocalNotifications} from "ionic-native"

@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html'
})
export class MessagesPage {
  messages:Array<{title:string,message:string,icon?:string}>=[
   {title:'No new message',message:'No new messages in your inbox'} 
  ];
  constructor(public navCtrl: NavController) {
      let db=new SQLite();
      db.openDatabase({
        name: 'akjistore.db',
        location: 'default' // the location field is required
      }).then(() => {
           db.executeSql('SELECT * FROM tblmessage', []).then((rs)=> {
             if(rs.rows.length>0){
                this.messages=[];
                for(var a=0;a<rs.rows.length;a++){
                  console.log(rs.rows.item(a));
                  this.messages.push({title:rs.rows.item(a).title,message:rs.rows.item(a).message,icon:rs.rows.item(a).icon})
                  db.executeSql('UPDATE tblmessage SET read=?', ['1']);    
                  LocalNotifications.clearAll().then(()=>{
                    console.log("Done clearing notifications...");
                  });              
                }
             }
            }, (error)=> {              
                  console.log('Unable to execute sql: '+ error);
            });
      });
  }
  ionViewDidLoad() {
    
  }

}
