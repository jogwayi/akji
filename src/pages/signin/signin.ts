import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { NavController,NavParams,Loading,ToastController, LoadingController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { SecurityQuestionPage } from '../security-question/security-question';
import { HttpRequestService } from '../../providers/http-request';
import { LogInPage } from '../login/login';
import {SQLite } from "ionic-native"

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
  host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateY(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate(700)
      ]),
      transition('* => void', animate(700, style({transform: 'translateY(-100%)', opacity: 0})))
    ])
  ]
})

export class SignInPage {

  form: FormGroup;

  //variables
  submitted:boolean = false;
  mobileNumber:string='';
  emailAddress:string='';
  countryCode:string;
  pageFrom:string='';
  loader:Loading;
  firstname:string=''
  lastname:string=''
  constructor(private toastCtrl: ToastController,private loadingCtrl:LoadingController,private params:NavParams, private httpService:HttpRequestService,public navCtrl: NavController,private formBuilder: FormBuilder) {
    this.mobileNumber =params.data['mobileNumber'];
    this.emailAddress = params.data['emailAddress'];
    this.countryCode=params.data['countryCode'];
    this.pageFrom=params.data['from'];
    this.form = this.formBuilder.group({
      mobileNumber: [this.mobileNumber, Validators.compose([Validators.required])],
      appPassword: ["", Validators.compose([Validators.required])],
      confirmPassword:  ["", Validators.compose([Validators.required])]
    });
    let db=new SQLite();
      db.openDatabase({
        name: 'akjistore.db',
        location: 'default' // the location field is required
      }).then(() => {
           db.executeSql('SELECT * FROM tbluser', []).then((rs)=> {
             if(rs.rows.length>0){
                this.firstname=rs.rows.item(0).firstname;                
                this.lastname=rs.rows.item(0).lastname;                
                this.emailAddress=rs.rows.item(0).email;                
                this.mobileNumber=rs.rows.item(0).phone;                
             }
            }, (error)=> {              
                  console.log('Unable to execute sql: '+ error);
            });
      });
  }
  get isReadonly() {return true;}

  ionViewDidLoad() {
    
  }

  next(){
    if(this.form.valid){
      this.submitted = true;
      this.loader = this.loadingCtrl.create({
        content: "Saving your password...",
      });      
      this.httpService.createPassword({
        MobileNumber:this.form.get('mobileNumber').value,
        Password:this.form.get('appPassword').value,
        ConfirmPassword:this.form.get('confirmPassword').value
      },{}).subscribe(res=>{
        if(this.pageFrom ==='ForgotPassword'){
          this.goToLogin();
        }else{
          if(res.Data && res.Data.ActionStatus){
              this.loadSecurityQuestions();
          }else{
            this.submitted = false;
            let toast = this.toastCtrl.create({
                message: res.Data.ActionMessage || res.Message,
                position: 'bottom',
                showCloseButton:true
              });
            toast.present();
          }
        }
          this.submitted =false
      })
    }else{      
      this.loader.dismiss()
    }
    
  }
  goToLogin(){
      this.loader.dismiss()
      this.navCtrl.setRoot(LogInPage,{mobileNumber:this.mobileNumber});
  }
  loadSecurityQuestions(){    
    this.loader.setContent("Loading security questions...")
    this.httpService.getSecurityQuestions({        
      },{}).subscribe(res=>{   
          this.loader.dismiss()       
          this.submitted =false
          this.navCtrl.setRoot(SecurityQuestionPage,{countryCode:this.countryCode,emailAddress:this.emailAddress,securityQuestions:res.Data.SecurityQuestions,mobileNumber:this.mobileNumber});
      })
  }

}
