import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {SQLite } from 'ionic-native';
import { HomePage } from '../home/home'

class ZoneLeaders{
      ZoneID: string;
      ZoneLeader: string;
      ZoneName: string;
      MobileNo: string;
      AdditionalMobileNo:string;
      constructor(options:{ ZoneID: string;
      ZoneLeader: string,
      MobileNo: string,
      ZoneName?:string,
      AdditionalMobileNo:string}){
        this.ZoneID=options.ZoneID;
          this.ZoneLeader=options.ZoneLeader;
          this.MobileNo=options.MobileNo;
          this.ZoneName=options.ZoneName;
          this.AdditionalMobileNo=options.AdditionalMobileNo;
      }
}

@Component({
  selector: 'page-use-card',
  templateUrl: 'use-card.html'
})
export class UseCardPage {

  //variables
  activeMenu:number;
  title:string;
  userData={};
  ZoneLeaders:Array<ZoneLeaders>=[];
  ZoneName:string='';
  constructor(public navCtrl: NavController, public params:NavParams) {
    this.activeMenu=params.data['activeMenu'];
    this.ZoneLeaders=params.data['ZoneLeaders']; 
    console.log(this.ZoneLeaders)
     
     try{
       let db=new SQLite();
      db.openDatabase({
        name: 'akjistore.db',
        location: 'default' // the location field is required
      }).then(() => {
           db.executeSql('SELECT * FROM tbluser',{}).then((rs) => {
             if(rs.rows.length>0){
              console.log(rs.rows.item(0));
              this.userData=rs.rows.item(0);
              this.ZoneName=rs.rows.item(0).ZoneName;
            } 
          });
      });
     }catch(e){
       console.log(e);
     }
    
    
  }

  ionViewDidLoad() {
    console.log("Retrieved : " + this.activeMenu);
    switch(this.activeMenu){
      case 1: this.title="Security"; break;
      case 3: this.title="My Account"; break;
    }
  }

  back(){
    this.navCtrl.setRoot(HomePage);
  }

}
