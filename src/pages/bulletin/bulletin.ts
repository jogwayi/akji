import { Component } from '@angular/core';
import { NavController,NavParams,Loading,LoadingController } from 'ionic-angular';
import { BulletinDetailPage } from '../bulletin-detail/bulletin-detail';
import { HttpRequestService } from '../../providers/http-request'; 

export class Bulletins{
      ViewId: number;
      ViewShortDesc:string;
      HasDetail:string;
      BulletinDescription:string;

      constructor(options:{ViewId: number,ViewShortDesc:string,
            HasDetail:string, BulletinDescription?:string}){
            this. ViewId =options.ViewId;
            this.ViewShortDesc=options.ViewShortDesc;
            this.HasDetail=options.HasDetail;
              this.BulletinDescription=options.BulletinDescription;


          }

}
@Component({
  selector: 'page-bulletin',
  templateUrl: 'bulletin.html'
})
export class BulletinPage {

  bulletins:Array<Bulletins>=[];
  title:string;
  loader:Loading;
  constructor(private loadingCtrl: LoadingController,private httpService:HttpRequestService,public navCtrl: NavController,private params:NavParams) {
      this.bulletins= params.data['bulletins'];
      this.title= params.data['title'];
  }
  ionViewDidLoad() {
    
  }
   showDetail(id:number,title:string,HasDetail:boolean){
     this.loader = this.loadingCtrl.create({content:'Loading bulletin...'});
     this.loader.present();
     if(HasDetail){
       this.httpService.getAllNormalEventsWithAllData({
          NewsletterId:id
        },{}).subscribe(res=>{   
            this.loader.dismiss();
            if(res.Data){
              this.navCtrl.push(BulletinDetailPage,{title:title,detail:res.Data.NewsBulletinDescription});
            }
      })
     }
  }
  stripTags(text){
    var text = text.replace(/<br>|<br\/>|<\/p>/g, "");
    //text = text.replace(/./g, ".\n");
    return text.replace(/<\/?[^>]+(>|$)/g, "");
  }

}
