import { Component } from '@angular/core';
import { NavController,LoadingController,Loading,Platform,ModalController,AlertController } from 'ionic-angular';
import { Device,Sim,Network,SQLite } from "ionic-native"
import { InternetMessageComponent } from '../../components/components-barrel';
import { LaunchPage } from '../launch/launch';
import { DataService } from '../../providers/data-service';
import { HttpRequestService } from '../../providers/http-request'; 
import { SignInPage } from '../signin/signin';
import { SignUpPage } from '../signup/signup';
import { LogInPage } from '../login/login';
import { HomePage } from '../home/home';
import { SecurityQuestionPage } from '../security-question/security-question';

@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html'
})
export class SplashPage {
  
  countryCode:any;
  countryMCC:any;
  imei:string;
  os:string;
  osVersion:string;
  deviceModel:string;
  serialNumber:string;
  emailAddress:string='';
  mobileNumber:string;
  mcc:Array<{code:string,countryCode:string,name:string}>=[
    {code:"639",countryCode:'254',name:'Kenya'},
    {code:"655",countryCode:'27',name:'South Africa'},
    {code:"642",countryCode:'257',name:'Burundi'},
    //{code:"652",countryCode:'27',name:'Botswana'},
    {code:"635",countryCode:'250',name:'Rwanda'},
  ]
  db:SQLite;
  constructor( private dataService:DataService ,private httpService:HttpRequestService,private platform: Platform,public navCtrl: NavController,public loadingCtrl: LoadingController,private modal:ModalController,private alertCtl:AlertController) {
      
  }
  currentAction:string;
  connectSubscription:any;
  loader:Loading;

  ionViewDidLoad() {    
    //navigator.splashscreen.hide();
    this.platform.ready().then(() => {
        this.imei =  Device.device.uuid;
        this.os =  Device.device.platform;
        this.osVersion =  Device.device.version;
        this.deviceModel =  Device.device.model;
        this.currentAction = 'Checking sim information.\nPlease wait...'
        this.loader = this.loadingCtrl.create({
          content: this.currentAction,
          //duration: 3000
        });
        this.loader.present(); 
            Sim.getSimInfo().then(
            (info) => {
                this.loader.setContent('Found a sim card, code: '+info.countryCode+'\n MCC:'+info.mcc);
                this.mcc.forEach(country=>{
                    if(country.code == info.mcc){
                        this.countryCode=country.countryCode
                        this.countryMCC=country.code
                    }
                });
                this.checkNetwork();
            },
            (err) =>{
            
              this.loader.setContent('Could not find a sim card');
              this.loader.dismiss();
            
              this.alertCtl.create(
              {
                title:"No SIM Card Found",
                message :"AKJI requires a device with a SIM card.",
                enableBackdropDismiss:false
              }).present();
          
          });
    }); 
  }

checkNetwork(){
    if(Network.connection == "none"){
        this.loader.setContent('No network connection was found!');
        this.loader.dismiss();
        let alertModal = this.modal.create(InternetMessageComponent,{});
        alertModal.present();
      }else{
        this.loader.setContent(Network.connection + ' network was found.');
        this.checkDB();
      }
  }

   checkDB(){

    this.db= new SQLite();
    let db=this.db;
    this.loader.setContent('Loading your information.\nPlease wait...');
    try {
      this.db.openDatabase({
        name: 'akjistore.db',
        location: 'default' // the location field is required
      }).then(() => {
          this.loader.setContent('Please wait...');//Loaded DB.\nPlease wait...
          //this.navCtrl.setRoot(LaunchPage,{});
          this.db.executeSql('SELECT * FROM tbluser', {}).then((rs)=> {
              this.loader.setContent('Welcome to AKJI');//Loaded Data
              this.loader.dismiss();
              console.log(rs.rows.item(0));
              console.log("Done checking data...")
              if(rs.rows.length ==0 || rs.rows.item(0).token===null  || rs.rows.item(0).token===undefined ){
                this.navCtrl.setRoot(LaunchPage,{imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC});
              }else{
                console.log("Good, now check registration status...")
                this.loader.setContent('Welcome');                //'Details were found for - '+rs.rows.item(0).phone                
                this.mobileNumber = rs.rows.item(0).phone;
                this.emailAddress = rs.rows.item(0).email;
                this.checkRegistrationStatus();
              }  
              this.db.executeSql(                
                  'UPDATE tbluser SET deviceModel=?,os=?,osVersion=?,serialNumber=?,imei=?,mcc=?,countryCode=?'
                ,[this.deviceModel,this.os,this.osVersion,this.serialNumber,this.imei,this.countryMCC,this.countryCode]
                ).then(()=>{
                  console.log("Successfuly updated device info");
                },(er)=>{
                  console.log("Updated error",er);
                });
          }, (error)=> {
            console.log("Some error here...")
              this.db.executeSql(                
                  'create table tbluser(fcmid VARCHAR(60),deviceModel VARCHAR(60),os VARCHAR(60),osVersion VARCHAR(60),serialNumber VARCHAR(60), token VARCHAR(32),authToken VARCHAR(32),imei VARCHAR(32),mcc VARCHAR(32),countryCode VARCHAR(32),name VARCHAR(32),phone VARCHAR(12),email VARCHAR(32),firstname VARCHAR(32),lastname VARCHAR(32),country VARCHAR(32),city VARCHAR(32),birthDay VARCHAR(32),street VARCHAR(32),area VARCHAR(32),zone VARCHAR(60),ZoneId VARCHAR(32),ZoneName VARCHAR(32),gender VARCHAR(32))'
                ,{}
                ).then(() => {
                  this.db.executeSql(                
                  'create table tblmessage(title VARCHAR(60),message VARCHAR(60),read VARCHAR(2),icon VARCHAR(96))',{});
                  db.executeSql(                
                    'INSERT INTO tbluser(deviceModel,os,osVersion,serialNumber,imei,mcc,countryCode) VALUES(?,?,?,?,?,?,?)',
                    [this.deviceModel,this.os,this.osVersion,this.serialNumber,this.imei,this.mcc,this.countryCode]
                  ).then(()=>{
                      console.log("Account set...done")
                  },(er)=>{
                    console.log("Account set... "+er)

                  });
                  
                  console.log("Created db... proceeding")                  
              }, (err) => {
                console.log('Unable to execute data: '+ err);
                this.loader.setContent('Unable to execute data: '+ err);
              });
              this.goToLaunchPage();
          }); 
      }, (err) => {
        this.loader.setContent('Unable to open database: '+ err);
      });
    } catch (error) {
        this.loader.setContent('Unable to open database: '+ JSON.stringify(error));
    }
    
  }
  goToLaunchPage(){
    this.loader.dismiss();
    console.log("Go to launch screen");
    this.navCtrl.setRoot(LaunchPage,{RegistrationStatusId:-1,imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber}).then(()=>{      
    },(error)=>{
      console.log("Got an error :" +error)
    });            
  }

  checkRegistrationStatus(){
    this.loader = this.loadingCtrl.create({
      content:"Verification was completed. \n\nChecking your details... ",
    });
      this.httpService.getRegistrationStatus({},{}).subscribe(res=>{  
        this.loader.dismiss();
        if(res.error){
            this.navCtrl.setRoot(LaunchPage,{RegistrationStatusId:0,imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
        }else{     
            switch(res.Data.RegistrationStatusId){
              case 0:
              case 1:
                this.navCtrl.setRoot(LaunchPage,{RegistrationStatusId:res.Data.RegistrationStatusId,imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
                break;
              case 2:
                  this.navCtrl.setRoot(SignUpPage,{imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
                break;
              case 3:                
                this.navCtrl.setRoot(SignInPage,{imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});               
                break
              case 4:
                this.loadSecurityQuestions();
                //this.navCtrl.setRoot(SignUpPage,{imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
                break
              default:              
                  this.checkLoginStatus();
                  //this.navCtrl.setRoot(HomePage,{imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC});
                break;
            }  
        }   
      })
  }
  loadSecurityQuestions(){
    this.loader = this.loadingCtrl.create({
          content: "Loading security questions, please wait...",
          //duration: 3000
        });
    this.loader.present();
    this.httpService.getSecurityQuestions({        
      },{}).subscribe(res=>{  
          this.loader.dismiss(); 
          this.navCtrl.setRoot(SecurityQuestionPage,{securityQuestions:res.Data.SecurityQuestions,imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
      })
  }
  checkLoginStatus(){
    this.httpService.getLoginStatus({},{}).subscribe(res=>{   
          if(res.Data && res.Data.ActionStatus){
            this.navCtrl.setRoot(HomePage,{imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
          }else{
            this.navCtrl.setRoot(LogInPage,{imei:this.imei,countryCode:this.countryCode,mcc:this.countryMCC,emailAddress: this.emailAddress,mobileNumber :this.mobileNumber});
          }
    })
  }

}
