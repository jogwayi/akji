import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import {NormalEvents} from "../events/events";


@Component({
  selector: 'page-events-detail',
  templateUrl: 'events-detail.html'
})
export class EventsDetailPage {

 title:string;
 event:NormalEvents;
  constructor(public navCtrl: NavController,private params:NavParams) {
      this.event= params.data['event'];   
      this.title= params.data['title'];   
  }
  ionViewDidLoad() {
     
  }

}
