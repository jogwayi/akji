import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';

/*
  Generated class for the NewsletterDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-newsletter-detail',
  templateUrl: 'newsletter-detail.html'
})
export class NewsletterDetailPage {
  newsletter:string;
  title:string;
  constructor(public navCtrl: NavController,private params:NavParams) {
      this.newsletter= params.data['newsletters'];   
      this.title= params.data['title'];   
  }

  ionViewDidLoad() {
     
  }

}
