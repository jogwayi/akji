import { Component } from '@angular/core';
import { NavController,NavParams,Loading,LoadingController } from 'ionic-angular';
import { HttpRequestService } from '../../providers/http-request'; 
import {InAppBrowser} from 'ionic-native';

export class NewsLetters{
      NewsletterId: number;
      NewsLetterName:string;
      ViewId:string;
      NewsLetterData:string;

      constructor(options:{NewsletterId?: number,NewsLetterName?:string,
            ViewId?:string, NewsLetterData?:string}){
            this. NewsletterId =options.NewsletterId;
            this.NewsLetterName=options.NewsLetterName;
            this.ViewId=options.ViewId;
            this.NewsLetterData=options.NewsLetterData;


          }

}
@Component({
  selector: 'page-newsletter',
  templateUrl: 'newsletter.html'
})
export class NewsletterPage {

  newsletters:Array<NewsLetters>=[];
  title:string;
  loader:Loading;
  browser:InAppBrowser;
  constructor(private loadingCtrl: LoadingController,private httpService:HttpRequestService,public navCtrl: NavController,private params:NavParams) {
      this.newsletters= params.data['newsletters'];
      this.title= params.data['title'];
  }
  ionViewDidLoad() {
    
  }
  showDetail(id:number,title:string){
      this.loader = this.loadingCtrl.create({});
      this.loader.present();
      this.loader.setContent("Fetching normal events...");        
      this.httpService.getNewsLetterData({
         NewsletterId:id
       },{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data){
            let content = this.stripTags(res.Data.NewsLetterData);
            console.log(content);
            this.showBrowser(content,(id)=>{
                  console.log(id);
              });
          }
    })
  }
   stripTags(text){
    text= text.replace(/<\/html>|<html[^>]*>|<!DOCTYPE[^>]*>/g, "");
    text= text.replace(/'/g, "\"");
    return text;
  }
  showBrowser(html:string,callback){
    html+="<style>img{width:320px;max-width:568px important;}</style>"
    this.browser = new InAppBrowser('assets/blank-old.html','_blank',"location=no");
    this.browser.on('loadstop').subscribe((event) => {
          this.browser.executeScript({code:"document.getElementById('htmlMakUp').innerHTML='"+html+"';"}).then((res)=>{
                console.log('Content rendered');
          },error => {
                console.log("InAppBrowser method error: " + error);
          })                
          if (event.url.match("data/")) {
              console.log(event.url)
              this.browser.close();
              var id = event.url.split("?");
              if(id[id.length-1] !=="close"){
                callback(id[id.length-1]);
              }
          }
        },
        err => {
        console.log("InAppBrowser Loadstop Event Error: " + err);
    });
    this.browser.show();
  }

}
