import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { NavController,NavParams,Loading,LoadingController,ToastController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { HomePage } from '../home/home';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { HttpRequestService } from '../../providers/http-request';
import {SQLite } from "ionic-native"

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateY(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate(700)
      ]),
      transition('* => void', animate(700, style({transform: 'translateY(-100%)', opacity: 0})))
    ])
  ]
})
export class LogInPage {

  form: FormGroup;

  submitted:boolean = false;
  firstname:string=''
  lastname:string=''
  mobileNumber:string='';
  loader:Loading;
  emailAddress:string='';
  constructor(private toastCtrl: ToastController,private params:NavParams,private loadingCtrl: LoadingController, private httpService:HttpRequestService,public navCtrl: NavController,private formBuilder: FormBuilder) {
    this.mobileNumber =params.data['mobileNumber'];
    this.emailAddress =params.data['emailAddress'];
    this.form = this.formBuilder.group({
      mobileNumber: [this.mobileNumber, Validators.compose([Validators.required])],
      appPassword: ["", Validators.compose([Validators.required])],
    });
     let db=new SQLite();
      db.openDatabase({
        name: 'akjistore.db',
        location: 'default' // the location field is required
      }).then(() => {
           db.executeSql('SELECT * FROM tbluser', []).then((rs)=> {
             if(rs.rows.length>0){
                this.firstname=rs.rows.item(0).firstname;                
                this.lastname=rs.rows.item(0).lastname;                
                this.emailAddress=rs.rows.item(0).email;                
                this.mobileNumber=rs.rows.item(0).phone;  
                this.form.get('mobileNumber').setValue(rs.rows.item(0).phone);              
             }
            }, (error)=> {              
                  console.log('Unable to execute sql: '+ error);
            });
      });
  }

  get isReadonly() {return true;}
  
  ionViewDidLoad() {
     
  }
  resetPassword(){
     this.loader = this.loadingCtrl.create({
        content: "Requesting your security questions...",
      });
      this.loader.present();
    this.httpService.forgotPasswordGetQuestions({        
      },{}).subscribe(res=>{    
        this.loader.dismiss();
          this.navCtrl.push(ForgotPasswordPage,{ForgotPasswordQuestions:res.Data});
      })
  }
  
  next(){
    this.submitted = true;
    if(this.form.valid){
       this.loader = this.loadingCtrl.create({
        content: "Logging you in. \n\nPlease wait...",
      });
      this.httpService.login(
        {UserName:this.form.get('mobileNumber').value,
        Password:this.form.get('appPassword').value}
      ,{}).subscribe(res=>{
        console.log("Login Response: " + res.Data);
        if(res.Data.ActionStatus){
          let db=new SQLite();
           db.openDatabase({
              name: 'akjistore.db',
              location: 'default' // the location field is required
            }).then(() => {
              db.executeSql(
                    'UPDATE tbluser SET authToken=?,ZoneId=?,ZoneName=?',
                    [res.Data.AuthenticationToken,res.Data.ZoneId,res.Data.ZoneName]                  
                  ).then(() => {
                    this.navCtrl.setRoot(HomePage,{});
                }, (err) => {
                  console.log("Could not save new login details");
                //this.loader.setContent('Unable to execute data: '+ err);
              });
              this.navCtrl.setRoot(HomePage,{});
           }, (err) => {
            //this.loader.setContent('Unable to open database: '+ err);
          });
          this.submitted = false;
        }else{
            this.submitted=false ;
              let toast = this.toastCtrl.create({
                message: res.Data.ActionMessage || res.Message,
                position: 'bottom',
                showCloseButton:true
              });
              toast.present();
        }

          this.loader.dismiss();
      })
    }
  }
}
