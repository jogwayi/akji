import { Component/*, trigger, state, style, transition, animate*/ } from '@angular/core';
import { Http} from '@angular/Http';
import { NavController,Loading,LoadingController,Events } from 'ionic-angular';
import { UseCardPage } from '../use-card/use-card';
import { HttpRequestService } from '../../providers/http-request'; 
import { NormalEvents } from '../events/events';
import { NewsletterPage } from '../newsletter/newsletter';
import { Bulletins } from '../bulletin/bulletin';
import { LogInPage } from '../login/login';
import {InAppBrowser,SQLite} from 'ionic-native';
import { MessagesPage } from '../messages/messages';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  /*host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0) rotateY(-360deg)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(100%) rotateY(0)', opacity: 0}),
        animate(1500)
      ]),
      transition('* => void', animate(1500, style({transform: 'translateX(-100%) rotateY(180deg)', opacity: 0})))
    ])
  ]*/
})

export class HomePage{
  verifyMe:boolean=false;
  activeMenu:number;
  loader:Loading;
  browser:InAppBrowser;
  unreadMessages:number=0;
  ZoneId:string='';
  menu:Array<{icon:string,title:string,action?:string,submenu?:Array<{title:string,icon:string,action:string}>,messageBadge?:boolean}>
  constructor(private eventListener:Events,private loadingCtrl: LoadingController,private httpService:HttpRequestService,public navCtrl: NavController,private http:Http) {

    this.menu = [
      {
        icon:'home.png',
        title:'Ismaili Kenya', 
        /*submenu:[{
              icon:'event.png',
              title:'Events', 
              action:'',
            },
            {
              icon:'calendar.png',
              title:'Majalis Calendar', 
              action:'',
            },{
              icon:'bulletin.png',
              title:'Bulletin', 
              action:'',
            },{
              icon:'newsletter.png',
              title:'Newsletter', 
              action:'',
            }
        ]*/
        action:''
      },
      {
        icon:'security.png',
        title:'Security',
        action:''      
      },
      {
        icon:'message.png',
        title:'Messages',
        action:'' ,
        messageBadge:true     
      },
      {
        icon:'account.png',
        title:'My Account',
        action:''
      },
    ];
    let db=new SQLite();
        db.openDatabase({
          name: 'akjistore.db',
          location: 'default' // the location field is required
        }).then(() => {
            db.executeSql('SELECT ZoneId FROM tbluser', []).then((rs)=> {
              if(rs.rows.length>0){
                  this.ZoneId  = rs.rows.item(0).ZoneId;
              }
            }, (error)=> {              
                    console.log('Unable to execute sql: '+ error);
            });
      });
    this.eventListener.subscribe('user:notification', (userEventData) => {
        let db=new SQLite();
        db.openDatabase({
          name: 'akjistore.db',
          location: 'default' // the location field is required
        }).then(() => {
            db.executeSql('SELECT COUNT(*) AS unread FROM tblmessage WHERE read=?', ['1']).then((rs)=> {
              if(rs.rows.length>0){
                  this.unreadMessages  = rs.rows.item(0).unread;
              }
              }, (error)=> {              
                    console.log('Unable to execute sql: '+ error);
              });
        });
      });
  }
  ngOnInit(){

  }
  showSubmenu(mainInd,SubIndex){
      this.loader = this.loadingCtrl.create({});
      this.loader.present();
      switch (mainInd){
      case 0: 
            switch (SubIndex){
                case 0:
                  this.loader.setContent("Fetching normal events...");
                  this.events();
                  break;
                case 1:
                  this.loader.setContent("Fetching majlis events...");
                  this.majlis();
                  break;
                case 2:
                  this.loader.setContent("Fetching bulletins...");
                  this.bulletin();
                  break;
                case 3:
                  this.loader.setContent("Fetching newsletters...");
                  this.newsletter();
                  break;

            }
        break;        
      default:
        break;
    }
  }
  events(){
      this.httpService.getNormalEvents({},{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data.NormalEvents){
            this.formatEventData('Events',res.Data.NormalEvents,(id)=>{
              this.showNormalEventDetails(id);
            });
            //this.navCtrl.push(EventsPage,{title:'Normal Events',events:res.Data.NormalEvents});
          }
    })
  }
  getDashBoardContet(){
    this.loader = this.loadingCtrl.create({content:'Fetching content...'});
      this.loader.present();      
      this.httpService.getCompleteDashboardHTML({},{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data){
            let content = this.stripTags(res.Data.DashboardHTML);
            console.log(content);
            this.showBrowser(content,(id)=>{
                  console.log(id);
              });
          }
    })
  }
  eventClicked(id){
    console.log(id);
  }
  formatEventData(title:string='Events',data:Array<NormalEvents>,callback:any){
    var html='';
    html=this.addHTMLHeader(html);
    html+='<div class="eventBg">\
            <h2>'+title+'</h2>\
            <div class="eventListWrap eventListing mCustomScrollbar"><ul>';
      data.forEach(content=>{
          var eventDate = new Date(content.EventDate).toUTCString();
          html+='<li>\
                <a href="/data/?'+content.EventID+'" data='+content.EventID+'">\
                      <span class="eventDate">'+eventDate+'</span>\
                      <span class="eventDes">'+content.EventDescription+'</span>\
                  </a>\
              </li>';
      });
      html+='</ul>\
            </div>\
        </div>';
    html=this.addHTMLFooter(html);
    this.showBrowser(html,callback);
  }
  formatBulletinData(title:string='Events',data:Array<Bulletins>,callback:any){
    var html='';
    html=this.addHTMLHeader(html);
      data.forEach(content=>{
        var detail='';
        if(content.HasDetail){
             detail='<div GUID="Box3" class="row GUIDAddButton">\
                <div class="cols12">\
                    <a href="/data/?'+content.ViewId+' cmd="Adddialogmoda" target="_self" title="Add" class="modalPopup Adddialogmodal" GUID="Box3">Add</a>\
                </div>\
              </div>';
            }
          html+=html+='<Box3>\
            <div class="commmonContentBox contentWithImg fixReadBtn" RowName="BoxRow" DeleteId="Box3" BoxName="Box3" cssSelector="editDelDiv">\
            '+content.ViewShortDesc+
            '</div>\
            '+detail+'</Box3>';
      });

    html=this.addHTMLFooter(html);
    this.showBrowser(html,callback);
  }
  formatEventDetailData(title:string='Events',content:NormalEvents,callback:any){
    var html='';
    console.log(content);
    html=this.addHTMLHeader(html);
    html+='<div class="eventBg">\
            <h2>'+content.EventTitle+'</h2>\
            <div class="eventListWrap eventListing mCustomScrollbar"><ul>';
          var startDate=content.EventDate || content.StartDateTime;
          var endDate=content.EndDateTime;
          var eventDate = new Date(startDate).toUTCString();
          var eventEndDate = new Date(endDate).toUTCString();
          html+='<li>\
                <a>\
                      <span class="eventDate">'+eventDate+'</span>\
                      <span class="eventDes">'+content.EventDescription+'</span>\
                  </a>\
              </li><li>\
                <a>\
                  <span class="eventDate">Duration in Hours</span>\
                  <span class="eventDes">'+content.DurationInHrs+'</span>\
                  </a>\
              </li><li>\
                <a>\
                      <span class="eventDate">Thematic  Area</span>\
                      <span class="eventDes">'+content.ThematicArea+'</span>\
                  </a>\
              </li><li>\
                <a>\
                      <span class="eventDate">Event Type</span>\
                      <span class="eventDes">'+content.EventType+'</span>\
                  </a>\
              </li><li>\
                <a>\
                      <span class="eventDate">City</span>\
                      <span class="eventDes">'+content.City+'</span>\
                  </a>\
              </li><li>\
                <a>\
                      <span class="eventDate">Confirmation</span>\
                      <span class="eventDes">'+content.City+'</span>\
                  </a>\
              </li><li>\
                <a>\
                      <span class="eventDate">Committee</span>\
                      <span class="eventDes">'+content.Committee+'</span>\
                  </a>\
              </li>';
      html+='</ul>\
            </div>\
        </div>';
    html=this.addHTMLFooter(html);
    this.showBrowser(html,callback);
  }
  showNormalEventDetails(id){
    this.httpService.getNormalEventDetail({
              EventID:id
          },{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data){
            this.formatEventDetailData(res.Data.EventTitle,res.Data,(id)=>{

            });
            //this.navCtrl.push(EventsPage,{title:'Normal Events',events:res.Data.NormalEvents});
          }
    })
    console.log(id);
  }
  
  showBulletinDetails(id){
    this.httpService.getBulletinViewDescriptionByViewId({
              EventID:id
          },{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data){
            var html =this.addHTMLHeader(html);
            html+=res.Data.BulletinDescription;
            html=this.addHTMLFooter(html);
            this.showBrowser(html,()=>{});            
            
          }
    })
    console.log(id);
  }
  showMajlisEventDetails(id){
    this.httpService.getMajlisEventDetails({
              EventID:id
          },{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data){
            this.formatEventDetailData(res.Data.EventTitle,res.Data,(id)=>{

            });
            //this.navCtrl.push(EventsPage,{title:'Normal Events',events:res.Data.NormalEvents});
          }
    })
  }
  majlis(){    
      this.httpService.getMajlisEvents({},{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data.MajlisEvents){
              this.formatEventData('Majlis Events',res.Data.MajlisEvents,(id)=>{
                  this.showMajlisEventDetails(id);
              });
            //this.navCtrl.push(EventsPage,{title:'Majlis Events',events:res.Data.MajlisEvents});
          }
    })
  }
  bulletin(){
    this.httpService.getBulletinViews({},{}).subscribe(res=>{  
          this.loader.dismiss(); 
          if(res.Data.Bulletins){
            this.formatBulletinData('Bulletins',res.Data.Bulletins,(id)=>{
                  this.showBulletinDetails(id);
              });
            //this.navCtrl.push(BulletinPage,{title:'Bulletins',bulletins:res.Data.Bulletins});
          }
    })
  }
  newsletter(){
    this.httpService.getNewsLetterNames({},{}).subscribe(res=>{   
          this.loader.dismiss();
          if(res.Data.NewsLetterNames){
            this.navCtrl.push(NewsletterPage,{title:'Newsletters',newsletters:res.Data.NewsLetterNames});
          }
    })
  }
  activateMenu(index:number){
    if(this.activeMenu == index){
      this.activeMenu =undefined;
    }else{
      this.activeMenu = index;
    }
    switch (index){
      case 0: 
        this.getDashBoardContet();
      break;        
      case 1: 
        this.loadZone();
      break;
      case 2:
        this.navCtrl.push(MessagesPage,{}); 
      break;
      case 3: 
        this.navCtrl.push(UseCardPage,{activeMenu:this.activeMenu}); 
       break;
    }
  }

  loadZone(){
    this.loader = this.loadingCtrl.create({content:'Logading information...'});
    this.loader.present();
    this.httpService.getZoneLeaderByZoneId({
      ZoneId:this.ZoneId
    },{}).subscribe(res=>{  
      this.loader.dismiss();      
      this.navCtrl.push(UseCardPage,{activeMenu:this.activeMenu,ZoneLeaders:res.Data.ZoneLeaders});
    });
      
    
  }
 /*stripTags(text){
    return text.replace(/<img[^>]*>|<html[^>]*>|<!DOCTYPE[^>]*>/g, "");
  }*/
   stripTags(text){
    text= text.replace(/<\/html>|<html[^>]*>|<!DOCTYPE[^>]*>/g, "");
    text= text.replace(/'/g, "\"");
    return text;
  }
  showBrowser(html:string,callback){
    this.browser = new InAppBrowser('assets/blank-old.html','_blank',"location=no");
    this.browser.on('loadstop').subscribe((event) => {
          console.log(event);
          /*this.browser.executeScript({code:"document.getElementById('browserTitle').innerHTML='Events';"}).then((res)=>{
                console.log('Title rendered');
          },error => {
                console.log("InAppBrowser method error: " + error);
          })*/       
          this.browser.executeScript({code:"document.getElementById('htmlMakUp').innerHTML='"+html+"';"}).then((res)=>{
                console.log('Content rendered');
          },error => {
                console.log("InAppBrowser method error: " + error);
          })       
          if (event.url.match("data/")) {
              console.log(event.url)
              this.browser.close();
              var id = event.url.split("?");
              if(id[id.length-1] !=="close"){
                this.loader = this.loadingCtrl.create({});
                this.loader.present();
                callback(id[id.length-1]);
              }
          }
        },
        err => {
        console.log("InAppBrowser Loadstop Event Error: " + err);
    });
    this.browser.show();
  }
  addHTMLHeader(html){
    html+='<head>\
              <meta charset="utf-8">\
              <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">\
              <title>ISMAILI KENYA</title>\
              <link href="http://akji-kenya.org/Content/front_materialdesignicons.min.css" rel="stylesheet"/>\
              <link href="http://akji-kenya.org/Content/front_style.css" rel="stylesheet"/>\
              <link href="http://akji-kenya.org/Content/front_responsive.css" rel="stylesheet"/>\
          </head>\
          <body id="body">\
              <div id="wrapper">';
    return html;
  }
  addHTMLFooter(html){
    html+='</div>\
        </body>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/jquery-min.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/jquery.unobtrusive-ajax.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/jquery.validate.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/jquery.validate.unobtrusive.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/modernizr.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/jquery.mmenu.all.min.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/json2.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/flaunt.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/Common.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/UrlConfig.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/TabCommon.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/modernizr.js"></script>\
        <script type="text/javascript" src="http://akji-kenya.org/Scripts/functions.js"></script>\
      <script type="text/javascript" src="http://akji-kenya.org/Scripts/general.js"></script>'
      return html;
  }
  ionViewDidEnter() {
        
  }
  logout(){
      this.loader = this.loadingCtrl.create({content:'Logging you out...'});
      this.loader.present();
      this.httpService.logOut({},{}).subscribe(res=>{  
          let db=new SQLite();
            db.openDatabase({
              name: 'akjistore.db',
              location: 'default' // the location field is required
            }).then(() => {
                db.executeSql('UPDATE tbluser SET authToken=?', [null]).then(()=> {
                  this.goToSignIn({});   
                  this.loader.dismiss();         
                },(error)=>{
                    console.log(error)
                });
            });
    })
  }
  goToSignIn(user){
    console.log("Going to sigin");
    this.navCtrl.setRoot(LogInPage,{});
  }
  
}
