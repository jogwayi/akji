import { Component, trigger, state, style, transition, animate } from '@angular/core';
import { NavController,NavParams, AlertController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { LogInPage } from '../login/login';
import { HttpRequestService } from '../../providers/http-request';

/*
  Generated class for the SecurityQuestion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-security-question',
  templateUrl: 'security-question.html'/*,
  host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateY(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate(700)
      ]),
      transition('* => void', animate(700, style({transform: 'translateY(-100%)', opacity: 0})))
    ])
  ]*/
})
export class SecurityQuestionPage {
   
  //variables
  submitted:boolean = false;

  form: FormGroup;
  securityQuestions:Array<{SecurityQuestionID:number,SecurityQuestion:string}>=[];
  mobileNumber:string;
  emailAddress:string;
  countryCode:string;
  constructor(private params:NavParams, private httpService:HttpRequestService,public navCtrl: NavController, public alertCtrl: AlertController, private formBuilder: FormBuilder) {
    this.securityQuestions = params.data['securityQuestions'];
    this.mobileNumber=params.data['mobileNumber'];
    this.emailAddress=params.data['emailAddress']
    this.countryCode=params.data['countryCode'];
    this.form = this.formBuilder.group({
      questionOne: ["", Validators.compose([Validators.required])],
      responseOne: ["", Validators.compose([Validators.required])],
      questionTwo: ["", Validators.compose([Validators.required])],
      responseTwo: ["", Validators.compose([Validators.required])],
      questionThree: ["", Validators.compose([Validators.required])],
      responseThree: ["", Validators.compose([Validators.required])]
    });

  }
 
  ionViewDidLoad() {
    
  }

  next(){
    this.submitted = true;
    console.log({SecurityQuestionId1:this.form.get('questionOne').value,
        SecurityAnswer1:this.form.get('responseOne').value,
        SecurityQuestionId2:this.form.get('questionTwo').value,
        SecurityAnswer2:this.form.get('responseTwo').value,
        SecurityQuestionId3:this.form.get('questionTwo').value,
        SecurityAnswer3:this.form.get('responseThree').value
        });
    if(this.form.valid){
        

      this.httpService.submitSecurityQuestion(
        {SecurityQuestionId1:this.form.get('questionOne').value,
        SecurityAnswer1:this.form.get('responseOne').value,
        SecurityQuestionId2:this.form.get('questionTwo').value,
        SecurityAnswer2:this.form.get('responseTwo').value,
        SecurityQuestionId3:this.form.get('questionThree').value,
        SecurityAnswer3:this.form.get('responseThree').value
        },{}).subscribe(res=>{
          this.submitted =false
          this.loadCountries();
      })
    }else{
      this.submitted  = false;
    }
  }

loadCountries(){
    this.httpService.getAllCountries({        
      },{}).subscribe(res=>{          
          this.submitted =false
          this.navCtrl.setRoot(LogInPage,{countryCode:this.countryCode,emailAddress:this.emailAddress,mobileNumber:this.mobileNumber,Countries:res.Data.Countries});
      })
  }
}
