import { Input,Component } from '@angular/core';

export declare var google:any;

@Component({
  selector: 'component-map',
  template:''
})
export class GooglePlaces {
  @Input() mapName;
  @Input() country;
  @Input() result;
  @Input() filterField:string;
  map:any;
  autocomplete:any;
  places:any;
  
  constructor() {  
     
  }

  ionViewDidLoad() {
    this.initMap(); 
  }
  initMap() {
    this.map = new google.maps.Map(document.getElementById(this.mapName), {
      zoom: this.country.zoom,
      center: this.country.center,
      mapTypeControl: false,
      panControl: false,
      zoomControl: false,
      streetViewControl: false
    });
    this.setAutoComplete();
    console.log(this.country);
  }

  setAutoComplete(){
    let country = {'country':this.country.code || 'ke'};
    this.autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */ (
                document.getElementById(this.filterField)), {
              //types: ['(street_address)'],
              componentRestrictions: country
            });
        this.places = new google.maps.places.PlacesService(this.map);

        this.autocomplete.addListener('place_changed', ()=>{
            var place = this.autocomplete.getPlace();
            this.result =place.name;
            console.log(place);
        });
  }

}
