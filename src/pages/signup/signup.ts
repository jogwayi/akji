import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { NavController,ToastController, ModalController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SignInPage } from '../signin/signin';
import { LogInPage } from '../login/login';
import { SecurityQuestionPage } from '../security-question/security-question';import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Http} from '@angular/Http';
import { HttpRequestService } from '../../providers/http-request';
import {SQLite } from "ionic-native";
export declare var google:any;

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateY(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate(700)
      ]),
      transition('* => void', animate(700, style({transform: 'translateY(-100%)', opacity: 0})))
    ])
  ]
})
export class SignUpPage {

  //variables
  submitted:boolean = false;
  MobileNumber:string;
  EmailAddress:string;
  CountryCode:string;

  loader:Loading;
  form: FormGroup;
  Countries:Array<{CountryId:number,CountryName:string}>=[];
  Cities:Array<{CityId:number,CityName:string}>=[];
  Areas:Array<{AreaId:number,AreaName:string}>=[];
  Streets:Array<{StreetId:number,StreetName:string}>=[];
  Zones:Array<{ZoneId:number,Description:string}>=[];
  codes:Object={
    '254':1,
    '27':2,
    '250':3,
    '267':6,
    '264':5,
    '268':7,
    '257':4
  }
  country:{center: {lat: any, lng: any},zoom: number, code:string} = {
              center: {lat: -25.3, lng: 133.8},
                zoom: 4,
                code:'ke'
            };
  countryCoords:Object={
    '254':{
        center: {lat: -25.3, lng: 133.8},
        zoom: 4,
        code:'ke'
    },
    '27':{
        center: {lat: -25.3, lng: 133.8},
        zoom: 4,
        code:'ke'
    },
    '250':{
        center: {lat: -25.3, lng: 133.8},
        zoom: 4,
        code:'ke'
    },
    '267':{
        center: {lat: -25.3, lng: 133.8},
        zoom: 4,
        code:'ke'
    },
    '264':{
        center: {lat: -25.3, lng: 133.8},
        zoom: 4,
        code:'ke'
    },
    '268':{
        center: {lat: -25.3, lng: 133.8},
        zoom: 4,
        code:'ke'
    },
    '257':{
        center: {lat: -25.3, lng: 133.8},
        zoom: 4,
        code:'ke'
    }
  };
  placeholderMap:any;
  searchItem:string;
  displayCountries:boolean=true;
  streetValue:string='';
  map:any;
  autocomplete:any;
  places:any;
  constructor(public toastCtrl: ToastController,private params:NavParams, private httpService:HttpRequestService, public navCtrl: NavController, private http:Http, private modal:ModalController, private formBuilder: FormBuilder, public loadingCtrl: LoadingController) {
    this.Countries = params.data['Countries'];    
    this.MobileNumber = params.data['mobileNumber'];
    this.EmailAddress = params.data['emailAddress'];
    this.CountryCode = params.data['countryCode'];    
    this.country=this.countryCoords[this.CountryCode];
    console.log(this.country);

    this.form = this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      birthDate: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      country: [this.codes[this.CountryCode], Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      area: ['', Validators.compose([Validators.required])],
      zone: ['', Validators.compose([])],
      street: ['', Validators.compose([Validators.required])],  
    });

    if(this.codes[this.CountryCode] !==undefined){
      this.getCities();
      this.displayCountries=false;
    }
  }
  ionViewDidLoad() {
    console.log("Got here..... and here")
    let db=new SQLite();
    try{
      db.openDatabase({
          name: 'akjistore.db',
          location: 'default' // the location field is required
        }).then(() => {
          db.executeSql('SELECT * FROM tbluser', []).then((rs)=> {
              if(rs.rows.length>0){
                this.MobileNumber = rs.rows.item(0).phone;
                this.EmailAddress = rs.rows.item(0).email;
                this.form.setValue({
                   firstName: rs.rows.item(0).firstname || '',
                   lastName:rs.rows.item(0).lastname || '',
                   birthDate: rs.rows.item(0).birthDay || '',
                   gender: rs.rows.item(0).gender  || '',
                   country: this.codes[this.CountryCode] || '',
                   city: rs.rows.item(0).city || '',
                   area: rs.rows.item(0).area || '',
                   zone: rs.rows.item(0).zone || '',
                   street:rs.rows.item(0).street || '',
                });
                this.country=this.countryCoords[this.CountryCode];
              }  
          }, (error)=> {              
                  console.log('Unable to execute sql: '+ error);
            });
      },(error)=>{
        console.log("DB ERROR",error);
      });
    }catch(e){
      console.log(e);
    }
    this.initMap();

  }
  presentToast(message:string) {
    let toast = this.toastCtrl.create({
      message: message,
      position: 'bottom',
      showCloseButton:true
    });
    toast.present();
    toast.onDidDismiss(() => {
      console.log('Message received');
    });
    this.checkRegistrationStatus();
  }

  getCities(){
    this.loader = this.loadingCtrl.create({
        content: "Fetching Cities",
      });
    this.loader.present();
    this.httpService.getCitiesByCountryId({
          CountryId:this.form.get('country').value,
      },{}).subscribe(res=>{
         this.loader.dismiss();
         this.Cities=res.Data.Cities
      })
  }

  getArea(){
    this.loader = this.loadingCtrl.create({
        content: "Fetching areas",
      });
    this.loader.present();    
    this.httpService.getAreaByCityId({
          CityId:this.form.get('city').value,
      },{}).subscribe(res=>{
         this.loader.dismiss();
         this.Areas=res.Data.Areas
      })
  }

  getZone(){
    this.loader = this.loadingCtrl.create({
        content: "Fetching areas",
      });
    this.loader.present();    
    this.httpService.getZoneByZoneAreaId({
          CityId:this.form.get('city').value,
          AreaId:this.form.get('area').value,
      },{}).subscribe(res=>{
         this.loader.dismiss();
         this.Zones=res.Data.Zones
      })
  }

  getStreet(){
    /*this.loader = this.loadingCtrl.create({
        content:"Fetching streets",
      });
    this.loader.present();    
    this.httpService.getStreetByAreaId({
          AreaId:this.form.get('area').value,
      },{}).subscribe(res=>{
         this.loader.dismiss();
         this.Streets=res.Data.Streets
      })*/
  }
 
  pad(num, size) {
      var s = num+"";
      while (s.length < size) s = "0" + s;
      return s;
  }

  next(){
      this.submitted = true;
      //Show Alert dialog before saving
      this.loader = this.loadingCtrl.create({
        content:"Thank you for signing up.\n\nRegistering you... please wait",
      });
      this.loader.present();  

      let bdate = new Date(this.form.get('birthDate').value);
      console.log("birthDate : " + isNaN(bdate.getDate()));

      if (isNaN(bdate.getDate())){
        this.loader.dismiss();
        this.submitted = false;
        this.presentToast("Enter your date of birth."); 
      } else {

        if(this.form.valid){   
      
          /*if ( Object.prototype.toString.call(d) === "[object Date]" ) {
          // it is a date
          if ( isNaN( d.getTime() ) ) {  // d.valueOf() could also work
            // date is not valid
          }
          else {
            // date is valid
          }
        }
        else {
          // not a date
        }*/

        /*if (bdate===null || bdate===undefined || !bdate){
            this.loader.dismiss();
            this.submitted = false;
            this.presentToast("Enter your date of birth."); 
        } else*/ 

      try{
        this.httpService.registration({
          FirstName:this.form.get('firstName').value,
          LastName:this.form.get('lastName').value,
          DateOfBirth:this.pad(bdate.getDate(),2)+'/'+this.pad(bdate.getMonth()+1,2)+'/'+bdate.getFullYear(),// this.form.get('birthDate').value,
          CountryId:this.form.get('country').value,
          CityId:this.form.get('city').value,
          AreaId:this.form.get('area').value,
          Gender:this.form.get('gender').value,
          ZoneId:this.form.get('zone').value,
          StreetId:this.form.get('street').value,
          EmailAddress:this.EmailAddress,
          MobileNumber:this.MobileNumber
      },{}).subscribe(res=>{
            this.loader.dismiss();
            if(res.Data.ActionStatus){
              let db=new SQLite();
              db.openDatabase({
                  name: 'akjistore.db',
                  location: 'default' // the location field is required
                }).then(() => {
                  db.executeSql(
                      'UPDATE tbluser SET firstname=?,lastname=?,street=?,country=?,city=?,birthDay=?,area=?,ZoneId=?',
                      [this.form.get('firstName').value,this.form.get('lastName').value,this.form.get('street').value,
                        this.form.get('country').value,this.form.get('city').value,this.form.get('birthDate').value,this.form.get('area').value,this.form.get('zone').value]                  
                      ).then(() => {
                        this.navCtrl.setRoot(SignInPage,{mobileNumber:this.MobileNumber});
                      }, (err1) => {
                          this.navCtrl.setRoot(SignInPage,{mobileNumber:this.MobileNumber});
                      });                  
              }, (err) => {
                //this.loader.setContent('Unable to open database: '+ err);
              });
              
            }else{
              this.presentToast(res.Data.ActionMessage);
            }
            this.loader.dismiss();
            this.submitted = false
        })
      }catch(e){
        this.loader.setContent("Error: " + e);
      }
    
    }else{
      this.submitted = false;
      console.log(this.form.errors);
      if (this.form.get('firstName').value.length==0 || this.form.get('lastName').value.length==0 || 
            this.form.get('city').value.length==0 || this.form.get('area').value.length==0 || this.form.get('gender').value.length==0 || this.form.get('street').value.length==0){
              this.presentToast("All fields are mandatory");
              this.loader.dismiss();
      }
    }
  }
    
  }
  
  checkRegistrationStatus(){
      this.loader = this.loadingCtrl.create({
        content:"Checking your details....",
      });
      this.httpService.getRegistrationStatus({},{}).subscribe(res=>{  
        this.loader.dismiss();
        if(!res.error){
            switch(res.Data.RegistrationStatusId){
              case 0:
              case 1:
                break;
              case 3:                
                this.navCtrl.setRoot(SignInPage,{countryCode:this.CountryCode,emailAddress: this.EmailAddress,mobileNumber :this.MobileNumber});               
                break
              case 4:
                this.loadSecurityQuestions();
                break
              default:              
                  this.checkLoginStatus();
                break;
            }  
        }   
      })
  }

  loadSecurityQuestions(){
    this.loader = this.loadingCtrl.create({
          content: "Loading security questions, please wait...",
          //duration: 3000
        });
    this.loader.present();
    this.httpService.getSecurityQuestions({        
      },{}).subscribe(res=>{  
          this.loader.dismiss(); 
          this.navCtrl.setRoot(SecurityQuestionPage,{securityQuestions:res.Data.SecurityQuestions,countryCode:this.CountryCode,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
      })
  }
  checkLoginStatus(){
    this.httpService.getLoginStatus({},{}).subscribe(res=>{   
          if(res.Data && res.Data.ActionStatus){
            this.navCtrl.setRoot(HomePage,{});
          }else{
            this.navCtrl.setRoot(LogInPage,{emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
          }
    })
  }
  initMap() {
    this.map = new google.maps.Map(document.getElementById('streetgmp'), {
      zoom: this.country.zoom,
      center: this.country.center,
      mapTypeControl: false,
      panControl: false,
      zoomControl: false,
      streetViewControl: false
    });
    this.setAutoComplete();
    console.log(this.country);
  }
   setAutoComplete(){
    let country = {'country':this.country.code || 'ke'};
    this.autocomplete = new google.maps.places.Autocomplete(
             (
                document.getElementById('streetAddress').getElementsByTagName('input')[0]), {
              //types: ['(street_address)'],
              componentRestrictions: country
            });
        this.places = new google.maps.places.PlacesService(this.map);

        this.autocomplete.addListener('place_changed', ()=>{
            var place = this.autocomplete.getPlace();
            this.streetValue =place.name;
            console.log(place);
        });
  }
  
}
