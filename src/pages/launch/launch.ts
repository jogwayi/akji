import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { Http} from '@angular/Http';
import { Platform, NavController,ToastController,ModalController,NavParams,Loading,LoadingController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder} from '@angular/forms';
import { ChangeEmailComponent,MessageModalComponent,EmailVerificationComponent} from '../../components/components-barrel';
import { SignUpPage } from '../signup/signup';
import { HttpRequestService } from '../../providers/http-request';
import { SQLite, Device } from "ionic-native"
import { SignInPage } from '../signin/signin';
import { LogInPage } from '../login/login';
import { HomePage } from '../home/home';
import { SecurityQuestionPage } from '../security-question/security-question';

@Component({
  selector: 'page-launch',
  templateUrl: 'launch.html'/*,
   host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'"
   },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({transform: 'translateX(100%)', opacity: 0}),
        animate(1000)
      ]),
      transition('* => void', animate(1000, style({transform: 'translateX(-100%)', opacity: 0})))
    ])
  ]*/
})

export class LaunchPage {

  //variables
  verifyMe:boolean=false;
  submitted:boolean=false;
  ongoingVerification:any;
  CountryCode:string='254';
  DeviceId:string='34233253252532';
  form: FormGroup;
  loader:Loading;
  mcc:string='';
  fcmid:string;
  RegistrationStatusId:number;
  emailRegex = '^[A-Za-z0-9]+(\.[_A-Za-z0-9]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,15})$';
 
  constructor(private toastCtrl: ToastController,private params:NavParams,private httpService:HttpRequestService,public navCtrl: NavController,private http:Http,private modal:ModalController,private formBuilder: FormBuilder,public loadingCtrl: LoadingController,private platform:Platform) {
    this.DeviceId = params.data['imei'];
    this.mcc = params.data['mcc'];
    this.RegistrationStatusId = params.data['RegistrationStatusId'];
    //console.log(this.params.data);
    this.form = this.formBuilder.group({
      countryCode: [params.data['countryCode'], Validators.compose([Validators.required])],
      mobileNumber: ['', Validators.compose([Validators.required])],
      emailAddress:  ['', Validators.compose([Validators.required,Validators.pattern(this.emailRegex)])]
    });
    window['FCMPlugin'].getToken(
        (token)=>{
          this.fcmid = token;
        },
        (err)=>{
          console.log('error retrieving token: ' + err);
        }
      )
    
  }
  showVerify(){
    this.verifyMe = true;
  }

 ionViewDidEnter() {
    if(this.RegistrationStatusId==1){
      this.emailVerification();
    }
  }
 
  verify(title:string,message:string,done:boolean=false){
      let alertModal = this.modal.create(MessageModalComponent,{title:title,message:message});
      alertModal.present();
      alertModal.onDidDismiss((data)=>{
        if(!done){
         this.checkRegistrationStatus();
        }else{
          this.navCtrl.setRoot( SignUpPage,{countryCode:this.form.get('countryCode').value,mobileNumber:this.form.get('mobileNumber').value,emailAddress: this.form.get('emailAddress').value});
        }
      })
  }
  changeEmail(title?:string,message?:string){
      let alertModal = this.modal.create(ChangeEmailComponent,{title:title,message:message});
      alertModal.present();
      alertModal.onDidDismiss((data)=>{
         this.verify(data.title,data.message,true);
      })
  }
  emailVerification(title?:string,message?:string){
      let alertModal = this.modal.create(EmailVerificationComponent,{title:title,message:message,data:{CountryCode:this.form.get('countryCode').value,
          MobileNumber:this.form.get('mobileNumber').value,
          EmailAddress:this.form.get('emailAddress').value,
          fcmid:this.fcmid,
          mcc:this.mcc,
          DeviceId:this.DeviceId}});
          alertModal.present();
          alertModal.onDidDismiss((data)=>{
          console.log(data);
          if(data.changeMail && !data.done){
              this.changeEmail();
          }else{
            this.navCtrl.setRoot( SignUpPage,{countryCode:this.form.get('countryCode').value,mobileNumber:this.form.get('mobileNumber').value,emailAddress: this.form.get('emailAddress').value});
          }
      })
  }
  validatePhoneNumber(){
        let mobile:string =this.form.get('mobileNumber').value;
        if(mobile[0]!=='0'){
          this.form.get('mobileNumber').setValue("0"+this.form.get('mobileNumber').value);
        }

        if ((this.form.get('countryCode').value==254) && (this.form.get('mobileNumber').value.length!=10)){
            this.loader.dismiss();
            this.submitted = false;
            this.presentToast("Mobile Number should be 10 digits long."); 
        } else {

        this.httpService.validatePhoneNumber(
          {CountryCode:this.form.get('countryCode').value,
          MobileNumber:this.form.get('mobileNumber').value,
          EmailAddress:this.form.get('emailAddress').value,
          fcmid:this.fcmid,
          DeviceId:this.DeviceId}
        ,{}).subscribe(res=>{
            //this.loader.setContent(JSON.stringify(res));
            //this.loader.dismiss();
            try{
              if(this.DeviceId ==undefined){
                this.DeviceId = Device.device.uuid;
              }
              if(res.Data.ActionStatus){
                  let db=new SQLite();
                  db.openDatabase({
                    name: 'akjistore.db',
                    location: 'default' // the location field is required
                  }).then(() => {
                      db.executeSql("UPDATE tbluser SET fcmid=?, token=?,imei=?,mcc=?,countryCode=?,phone=?,email=?,firstname=?,lastname=?",
                          [this.fcmid, res.Data.DeviceToken,this.DeviceId,this.mcc,this.form.get('countryCode').value,this.form.get('mobileNumber').value,this.form.get('emailAddress').value,res.Data.FirstName,res.Data.LastName]
                      ).then(()=>{
                        console.log("Successful transaction");
                        console.log(res.Data);
                        this.loader.dismiss();
                        if(res.Data.UserStatus==1){
                          this.checkRegistrationStatus();
                        }else if(res.Data.UserStatus==2){
                            this.navCtrl.setRoot(SignUpPage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
                        }else {
                            this.verifyMe=false;
                            this.verify("Almost Finished!","We need to confirm your email address. We've sent an email to " + this.form.get('emailAddress').value + ". Please click the link in that message to activate your account");                          
                        }

                          //this.navCtrl.setRoot( SignUpPage,{countryCode:this.form.get('countryCode').value,mobileNumber:this.form.get('mobileNumber').value,emailAddress: this.form.get('emailAddress').value});
                      }, (err) => {
                          this.loader.setContent('Unable to open db: '+ JSON.stringify(err));
                    });
                  }, (err) => {
                    this.loader.setContent('Unable to open db: '+ JSON.stringify(err));
                  }); 
                                   
              } else {
                this.loader.dismiss();
                let toast = this.toastCtrl.create({
                  message: res.Data.ActionMessage || res.Message,
                  position: 'bottom',
                  showCloseButton:true
                });
                toast.present();                
              }
            }catch(e){
                 console.log("sql error"+e);
            }
            //this.navCtrl.setRoot(SignUpPage,{countryCode:this.form.get('countryCode').value,mobileNumber:this.form.get('mobileNumber').value,emailAddress: this.form.get('emailAddress').value});
            this.submitted = false;
        });
      }
  }

  next(){
    this.submitted = true;
    if(this.form.valid){        
          this.loader = this.loadingCtrl.create({
          content: "Validating your information...",
        });
        this.loader.present();   
        console.log('Valid form');             
        if(this.fcmid ===null || this.fcmid ==='' || this.fcmid ===undefined){
            window['FCMPlugin'].getToken(
                (token)=>{
                  this.fcmid = token;
                  this.validatePhoneNumber();
                  console.log('retrieve fcmid first ');
                },
                (err)=>{
                  console.log('error retrieving token: ' + err);
                }
              )
        }else{
          this.validatePhoneNumber();
        }
    } else {
      if (this.form.get('mobileNumber').value.length==0 && this.form.get('emailAddress').value.length==0){
        this.presentToast("All fields are required");
      } else if (this.form.get('mobileNumber').value.length==0) {
        this.presentToast("Enter your mobile number");
      } else if (this.form.get('emailAddress').value.length==0) {
        this.presentToast("Enter your email address");
      }

      this.submitted = false;
    }
  }

  checkRegistrationStatus(){
      this.httpService.getRegistrationStatus({},{}).subscribe(res=>{  
        if(res.error){
            this.presentToast(res.error.Message);
        }else{     
            switch(res.Data.RegistrationStatusId){
              case 0:
              case 1:
                this.emailVerification();
                //this.presentToast("Please confirm your email account.\n An email has been sent to your registered email. Please click the link in that message to activate your account.");
                //this.navCtrl.setRoot(LaunchPage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
                break;
              case 2:
                  this.navCtrl.setRoot(SignUpPage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
                break;
              case 3:                
                this.navCtrl.setRoot(SignInPage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});               
                break
              case 4:
                this.loadSecurityQuestions();
                //this.navCtrl.setRoot(SignUpPage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
                break
              default:              
                  this.checkLoginStatus();
                  //this.navCtrl.setRoot(HomePage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc});
                break;
            }  
        }   
      })
  }
  presentToast(message:string) {
    let toast = this.toastCtrl.create({
      message: message,
      position: 'bottom',
      showCloseButton:true
    });
    toast.present();
    toast.onDidDismiss(() => {
      console.log('Message received');
    });
  }
  loadSecurityQuestions(){
    this.loader = this.loadingCtrl.create({
          content: "Loading security questions, please wait...",
          //duration: 3000
        });
    this.loader.present();
    this.httpService.getSecurityQuestions({        
      },{}).subscribe(res=>{  
          this.loader.dismiss(); 
          this.navCtrl.setRoot(SecurityQuestionPage,{securityQuestions:res.Data.SecurityQuestions,imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
      })
  }
  checkLoginStatus(){
    this.httpService.getLoginStatus({},{}).subscribe(res=>{   
          if(res.Data && res.Data.ActionStatus){
            this.navCtrl.setRoot(HomePage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
          }else{
            this.navCtrl.setRoot(LogInPage,{imei:this.DeviceId,countryCode:this.form.get('countryCode').value,mcc:this.mcc,emailAddress: this.form.get('emailAddress').value,mobileNumber :this.form.get('mobileNumber').value});
          }
    })
  }
}
