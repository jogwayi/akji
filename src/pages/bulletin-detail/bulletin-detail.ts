import { Component } from '@angular/core';
import { NavController,Loading,NavParams } from 'ionic-angular';

/*
  Generated class for the BulletinDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-bulletin-detail',
  templateUrl: 'bulletin-detail.html'
})
export class BulletinDetailPage {

 title:string;
 detail:string;
  constructor(public navCtrl: NavController,private params:NavParams) {
      this.detail= params.data['detail'];   
      this.title= params.data['title'];   
  }
  ionViewDidLoad() {
     
  }

  stripTags(text){
    var text = text.replace(/<br>|<br\/>|<\/p>/g, "");
    text = text.replace(/./g, ".\n");
    return text.replace(/<\/?[^>]+(>|$)/g, "");
  }

}
