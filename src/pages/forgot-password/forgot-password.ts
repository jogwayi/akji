import { Component } from '@angular/core';
import { NavController,Loading,LoadingController, NavParams,ToastController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { SignInPage } from '../signin/signin';
import { HttpRequestService } from '../../providers/http-request';

@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {

  //variables
  submitted:boolean = false;
  form: FormGroup;
  changePasswordForm: FormGroup;
  loader:Loading;
  countryCode:string='';
  emailAddress:string='';
  mobileNumber:string='';
  ForgotPasswordQuestions:{SecurityQuestion1Id?:string,SecurityQuestion1?:string,SecurityQuestion2Id?:string,SecurityQuestion2?:string}={};
  constructor(private toastCtrl: ToastController,private params:NavParams, private httpService:HttpRequestService,public navCtrl: NavController, private formBuilder: FormBuilder,private loadingCtrl: LoadingController) {
    this.mobileNumber =params.data['mobileNumber'];
    this.emailAddress =params.data['emailAddress'];
    this.ForgotPasswordQuestions =params.data['ForgotPasswordQuestions'];
    
    this.form = this.formBuilder.group({
        responseOne: ["", Validators.compose([Validators.required])],
        responseTwo: ["", Validators.compose([Validators.required])]
    });
   /* this.changePasswordForm = this.formBuilder.group({
        OldPassword: ["", Validators.compose([Validators.required])],
        NewPassword: ["", Validators.compose([Validators.required])],
        NewConfirmPassword: ["", Validators.compose([Validators.required])]
    });*/

  }

  ionViewDidLoad() {
     
  }
  saveSecurityQuestion(){
    if(this.form.valid){
      this.loader = this.loadingCtrl.create({
          content: "Validating security questions...",
        });
        this.loader.present();
        this.httpService.forgotPasswordSubmitSecurityQuestion({    
          SecurityQuestion1Id:this.ForgotPasswordQuestions.SecurityQuestion1Id,
          Answer1:this.form.get('responseOne').value,
          SecurityQuestion2Id:this.ForgotPasswordQuestions.SecurityQuestion2Id,
          Answer2:this.form.get('responseTwo').value,
          },{}).subscribe(res=>{    
            this.loader.dismiss();      
              this.submitted =false
              if(res.Data.ActionStatus){
                this.navCtrl.setRoot(SignInPage,{from:'ForgotPassword',emailAddress:this.emailAddress,mobileNumber:this.mobileNumber});
              }else{
                let toast = this.toastCtrl.create({
                  message: res.Data.ActionMessage || res.error.Message,
                  position: 'bottom',
                  showCloseButton:true
                });
                toast.present();
            }
          })
      }else{
          let toast = this.toastCtrl.create({
            message: "Please fill in all the inputs",
            position: 'bottom',
            showCloseButton:true
          });
          toast.present();
      }
  }
}
