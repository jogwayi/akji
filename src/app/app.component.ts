import { Component } from '@angular/core';
import { Platform,Events } from 'ionic-angular';
import { StatusBar,SQLite, Splashscreen,LocalNotifications } from 'ionic-native';
import { SplashPage } from '../pages/splash/splash';

@Component({
  templateUrl: 'app.html',  
  /*styleUrls:[
    './app.css'
  ]*/
})
export class AKJIApp {
  rootPage = SplashPage;
  db:SQLite;
  constructor(private eventListener:Events,platform: Platform) {
    platform.ready().then(() => {
      //platform
      this.startFireBase();
      StatusBar.styleLightContent();
      //Splashscreen.hide();
      this.hideSplashScreen();
    });
  }

  hideSplashScreen() {
    if (Splashscreen) {
      setTimeout(() => {
        Splashscreen.hide();
      }, 100);
    }
  }

  startFireBase(){
    window['FCMPlugin'].getToken(
        (token)=>{
          console.log(token);
            this.startNotificationListen();
        },
        (err)=>{
          console.log('error retrieving token: ' + err);
        }
      )
  }

  startNotificationListen(){
    
    window['FCMPlugin'].subscribeToTopic('messages');
    window['FCMPlugin'].onNotification(
        (data)=>{
        console.log(JSON.stringify(data) );
          this.db= new SQLite();
              let db=this.db;
              try {
                this.db.openDatabase({
                  name: 'akjistore.db',
                  location: 'default' // the location field is required
                }).then(() => {
                    db.executeSql(                
                      'INSERT INTO tblmessage(title,message,read) VALUES(?,?,?)',
                      [data.title,data.description || data.message ,'1']
                    ).then(()=>{
                        console.log("Notification saved")
                        this.eventListener.publish('user:notification', '1');
                    },(err)=>{
                      console.log("Notification could not be saved... "+err)

                    });
            });
            }catch(er){
                console.log("Error saving, push notifications: "+er);
            }
            //console.log(JSON.stringify(data));
          if(data.wasTapped){
            //Notification was received on device tray and tapped by the user.
            console.log(JSON.stringify(data) );
          }else{
            //Notification was received in foreground. Maybe the user needs to be notified.
            LocalNotifications.schedule({
              id: 1,
              text: data.title,
              led: '004f35',
              data: JSON.stringify(data),
              badge: 12,
              icon: 'res://icon',
            });
          }
        },
        function(msg){
          console.log('onNotification callback successfully registered: ' + msg);
        },
        function(err){
          console.log('Error registering onNotification callback: ' + err);
        }
      );
  }

}
