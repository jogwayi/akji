import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/Http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { AKJIApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { LaunchPage } from '../pages/launch/launch';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { MessagesPage } from '../pages/messages/messages';
import { SignInPage } from '../pages/signin/signin';
import { SecurityQuestionPage } from '../pages/security-question/security-question';
import { SignUpPage } from '../pages/signup/signup';
import { GooglePlaces } from '../pages/signup/google-places';
import { LogInPage } from '../pages/login/login';
import { SplashPage } from '../pages/splash/splash';
import { EventsPage } from '../pages/events/events';
import { EventsDetailPage} from '../pages/events-detail/events-detail';
import { NewsletterPage } from '../pages/newsletter/newsletter';
import { NewsletterDetailPage} from '../pages/newsletter-detail/newsletter-detail';
import { BulletinPage } from '../pages/bulletin/bulletin';
import { BulletinDetailPage} from '../pages/bulletin-detail/bulletin-detail';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { UseCardPage } from '../pages/use-card/use-card';
import { HttpRequestService } from '../providers/http-request';
import { DataService } from '../providers/data-service';
import { MessageModalComponent,EmailVerificationComponent,ChangeEmailComponent,InternetMessageComponent} from '../components/components-barrel';

@NgModule({
  declarations: [
    AKJIApp,
    AboutPage,
    ContactPage,
    HomePage,
    LogInPage,MessagesPage,GooglePlaces,
    NewsletterPage,NewsletterDetailPage,BulletinPage,BulletinPage,EventsPage,EventsDetailPage,BulletinDetailPage,
    LaunchPage,SignInPage,SecurityQuestionPage,SignUpPage,SplashPage, ForgotPasswordPage, UseCardPage,
    MessageModalComponent,EmailVerificationComponent,ChangeEmailComponent,InternetMessageComponent
  ],
  imports: [
    HttpModule,
    IonicModule.forRoot(AKJIApp,{
      mode:"md"
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AKJIApp,
    AboutPage,
    ContactPage,
    HomePage,MessagesPage,
    LogInPage,BulletinDetailPage,GooglePlaces,
    NewsletterPage,NewsletterDetailPage,BulletinPage,BulletinPage,EventsPage,EventsDetailPage,
    LaunchPage,SignInPage,SecurityQuestionPage,SignUpPage,SplashPage,ForgotPasswordPage, UseCardPage,
    MessageModalComponent,EmailVerificationComponent,ChangeEmailComponent,InternetMessageComponent
  ],
  providers: [
      HttpRequestService,DataService,
      {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}
