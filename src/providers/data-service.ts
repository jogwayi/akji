import {Injectable} from '@angular/core';
import * as Rx from 'rxjs/Rx';
import { Http } from '@angular/http';
import { SQLite } from "ionic-native"
export declare var FCMPlugin: any;
@Injectable()
export class DataService {
  db: SQLite;
  constructor(public http: Http) {

  }
}

@Injectable()
export class EventsService {
  listeners:Object;
  events: Rx.Observable<any>;
  eventsSubject : Rx.Subject<any>;
    constructor() {
        this.listeners = {};
        this.eventsSubject = new Rx.Subject();

        this.events = Rx.Observable.from(this.eventsSubject);

        this.events.subscribe(
            ({name, args}) => {
                if (this.listeners[name]) {
                    for (let listener of this.listeners[name]) {
                        listener(...args);
                    }
                }
            });
    }

    on(name, listener) {
        if (!this.listeners[name]) {
            this.listeners[name] = [];
        }

        this.listeners[name].push(listener);
    }

    broadcast(name, ...args) {
        this.eventsSubject.next({
            name,
            args
        });
    }
}