import { Injectable } from '@angular/core';
import { Http,Headers,RequestMethod,RequestOptions,/*Request,*/Response } from '@angular/http';
import {DataService } from './data-service';
import {Observable} from 'rxjs/Observable'; 
import {PromiseObservable} from 'rxjs/observable/PromiseObservable';
import 'rxjs/add/operator/map';
import {SQLite,HTTP } from "ionic-native"


//export let baseUrl:string="http://202.131.117.224:9998/akji_webapi/api/";
export let baseUrl:string="http://www.akji-kenya.org:81/api/";
export var SinchClient:any;
@Injectable()
export class HttpRequestService {
  validatePhoneNumberURL:string="Registration/VerifyMobileNumber";
  registrationUrl:string="Registration/RegistrationSubmit";
  createPasswordURL:string="Registration/CreatePasswordSubmit";
  securityQuestionURL:string="Registration/SecurityQuestionSubmit";
  getRegistrationStatusURL:string="Registration/GetRegistrationStatus";
  changeEmailAndSendVerificationMailURL:string="Registration/ChangeEmailAndSendVerificationMail";
  resendVerificationMailURL:string="Registration/ResendVerificationMail";
  getAllCountriesURL:string="General/GetAllCountries";
  getCitiesByCountryIdURL:string="General/GetCitiesByCountryId";
  getAreaByCityIdURL:string="General/GetAreaByCityId";
  getStreetByAreaIdURL:string="General/GetStreetByAreaId";
  getSecurityQuestionsURL:string="General/GetSecurityQuestions";
  getLoginStatusURL:string="User/GetLoginStatus";
  loginURL:string="User/LoginSubmit";
  logOutURL:string="User/LogOutSubmit";
  changePasswordURL:string="User/ChangePasswordSubmit";
  forgotPasswordGetQuestionsURL:string="User/ForgotPasswordGetQuestions";
  forgotPasswordURL:string="User/ForgotPasswordSubmit";
  getBulletinViewsURL:string="DashboardView/GetBulletinViews";
  getBulletinViewDescriptionByViewIdURL:string="DashboardView/GetBulletinViewDescriptionByViewId";
  getNormalEventsURL:string="DashboardView/GetNormalEvents";
  getAllNormalEventsWithAllDataURL:string="DashboardView/GetAllNormalEventsWithAllData";
  getNormalEventDetailsURL:string="DashboardView/GetNormalEventDetails";
  getMajlisEventsURL:string="DashboardView/GetMajalisEvents";
  getMajlisEventDetailsURL:string="DashboardView/GetMajlisEventDetails";
  getNewsLetterNamesURL:string="DashboardView/GetNewsLetterNames";
  getNewsLetterDataURL:string="DashboardView/GetNewsLetterData";
  getCompleteDashboardHTMLURL:string="DashboardView/GetCompleteDashboardHTML";
  getZoneLeaderByZoneIdURL:string="User/GetZoneLeaderByZoneId";
  getZoneByZoneAreaIdURL:string="General/GetZoneByCityAreaId";
  
  constructor(private dataService:DataService ,public http: Http) {
     
  }
  getHeaders(withAuthHeaders:boolean,defaultHeader?:Headers):Headers{
       let headers = new Headers();
        if(defaultHeader !==undefined){
            headers = defaultHeader;
        }
        if(!withAuthHeaders){
          return headers;
        }
        let db=new SQLite();
        try{
          db.executeSql('SELECT * FROM tbluser', []).then((rs)=> {
                headers.append('AKJI-DeviceId',rs.rows.item(0).imei);
                headers.append('AKJI-DeviceToken',rs.rows.item(0).token);
                return headers;
            }, (error)=> {              
                  console.log('Unable to execute sql: '+ error);
            });
        }catch(e){
          console.log(e);
        }
      return headers;       
  }
  getAreaByCityId(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getAreaByCityIdURL);
  }
  getZoneByZoneAreaId(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getZoneByZoneAreaIdURL);
  }
  getZoneLeaderByZoneId(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getZoneLeaderByZoneIdURL);
  }
  getStreetByAreaId(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getStreetByAreaIdURL);
  }
  getCompleteDashboardHTML(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getCompleteDashboardHTMLURL);
  }
  getSecurityQuestions(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getSecurityQuestionsURL);
  }
  getLoginStatus(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getLoginStatusURL);
  }
  login(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.loginURL);
  }
  changeEmailAndSendVerificationMail(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.changeEmailAndSendVerificationMailURL);
  }
  resendVerificationMail(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.resendVerificationMailURL);
  }
  logOut(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.logOutURL);
  }
  forgotPasswordGetQuestions(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.forgotPasswordGetQuestionsURL);
  }
  forgotPasswordSubmitSecurityQuestion(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.forgotPasswordURL);
  }
  submitSecurityQuestion(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.securityQuestionURL);
  }
  getRegistrationStatus(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getRegistrationStatusURL);
  }
  getAllCountries(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getAllCountriesURL);
  }
  getCitiesByCountryId(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.getCitiesByCountryIdURL);
  }
  createPassword(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.createPasswordURL);
  }
  getBulletinViews(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getBulletinViewsURL);
  }
  getBulletinViewDescriptionByViewId(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getBulletinViewDescriptionByViewIdURL);
  }
  getNormalEvents(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getNormalEventsURL);
  }
  getAllNormalEventsWithAllData(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getAllNormalEventsWithAllDataURL);
  }
  getNormalEventDetail(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getNormalEventDetailsURL);
  }
  getMajlisEvents(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getMajlisEventsURL);
  }
  getMajlisEventDetails(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getMajlisEventDetailsURL);
  }
  getNewsLetterNames(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getNewsLetterNamesURL);
  }
  getNewsLetterData(data,header?:any){
      return this.doNativeHTTPRequest(data,baseUrl+this.getNewsLetterDataURL);
  }
  registration(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.registrationUrl);
  }

  validatePhoneNumber(data,header?:any){
    return this.doNativeHTTPRequest(data,baseUrl+this.validatePhoneNumberURL,false);    
  }
  performRequest(formData:any,link,withAuthHeaders:boolean=true,method:string='POST'):Observable<any> {    
    return PromiseObservable.create(new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();        
        xhr.open(method, link, true)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.response))
                } else {
                    reject(xhr.response)
                }
            }
        }
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
        if(withAuthHeaders){   
          xhr.setRequestHeader('AKJI-DeviceId','undefined');
          xhr.setRequestHeader('AKJI-DeviceToken','104456bd-4f9a-422e-87ca-723dfc3b4c4e');                           
            /*let db=new SQLite();
            try{
              db.executeSql('SELECT * FROM tbluser', []).then((rs)=> {
                    if(rs.rows.length>0){
                        xhr.setRequestHeader('AKJI-DeviceId',rs.rows.item(0).imei);
                        xhr.setRequestHeader('AKJI-DeviceToken',rs.rows.item(0).token);
                    }
                }, (error)=> {              
                      console.log('Unable to execute sql: '+ error);
                });
            }catch(e){
              console.log(e);
            }*/
            xhr.send(formData)
        }else{
            xhr.send(formData);
        }
    })) ;
  }
   doNativeHTTPRequest(data:any,link:string,withAuthHeaders:boolean=true,method:RequestMethod=RequestMethod.Post):Observable<any>{     
      console.log("gets here");
      return PromiseObservable.create(new Promise((resolve, reject) => {
        var deviceid,token,contentType,authid;
        if(withAuthHeaders){   
            deviceid ='';
            authid ='';
            token ='';                           
              let db=new SQLite();
              try{
                db.openDatabase({
                    name: 'akjistore.db',
                    location: 'default' // the location field is required
                  }).then(() => {
                    db.executeSql('SELECT * FROM tbluser', []).then((rs)=> {
                        if(rs.rows.length>0){
                          console.log(rs.rows.item(0));
                            deviceid=rs.rows.item(0).imei;
                            token=rs.rows.item(0).token;
                            authid=rs.rows.item(0).authToken;
                        }   
                        /*deviceid='1234567';
                        token='df6eaa94-1176-4fdf-9c71-4286a528fee5';
                        authid='f7f72bb3-c574-4a35-bcd2-0d72e82d133e';*/
                        console.log("Done");
                        contentType = 'application/json; charset=UTF-8';         
                        //console.log(link,data,{'AKJI-DeviceId':deviceid,'AKJI-DeviceToken':token,'Content-Type':contentType,'AKJI-AuthenticationToken':authid});
                        HTTP.post(link,data,{'AKJI-DeviceId':deviceid,'AKJI-DeviceToken':token,'Content-Type':contentType,'AKJI-AuthenticationToken':authid}).then((res)=>{
                          //console.log(res);
                          var response;
                          if(res.error){
                              response=JSON.parse(res.error);
                          }else{
                            response=JSON.parse(res.data);
                          }
                          response.responseHeaders = res.headers;
                          return resolve(response);
                        },(error) =>{
                                console.log(error);
                                resolve(error)
                        }); 
                      }, (error)=> {              
                            console.log('Unable to execute sql: '+ error);
                      });
                },(error)=>{
                  console.log("DB ERROR",error);
                });
              }catch(e){
                console.log(e);
              }
        }else{
          //console.log(link,data,{'AKJI-DeviceId':deviceid,'AKJI-DeviceToken':token,'Content-Type':contentType,'AKJI-AuthenticationToken':authid});
          HTTP.post(link,data,{'AKJI-DeviceId':deviceid,'AKJI-DeviceToken':token,'Content-Type':contentType,'AKJI-AuthenticationToken':authid}).then((res)=>{
                //console.log(res);
                var response;
                if(res.error){
                    response=JSON.parse(res.error);
                }else{
                  response=JSON.parse(res.data);
                }
                response.responseHeaders = res.headers;
                return resolve(response);
              },(error) =>{
                  var response=JSON.parse(error);
                  console.log(error);
                  resolve(response);
              });
        }


      }));
              
   }
  //The angular way, had issues with headers
  doHTTPRequest(data:any,link:string,withAuthHeaders:boolean=true,method:RequestMethod=RequestMethod.Post){
    let headers:Headers = this.getHeaders(withAuthHeaders);     
    headers.delete("Content-Type");
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');   
    var requestOptions = new RequestOptions({
        headers: headers,
    })               
        
    return this.http.post(link,data,requestOptions)
          .map((res: Response) => {
                //console.log(res);
                if (res) {
                    return res.json();
                }
              })
    /*return this.http.request(new Request(requestOptions))
        .map((res: Response) => {
          console.log(res);
            if (res) {
                return { status: res.status, json: res.json() }
            }
        })*/
  }

}
